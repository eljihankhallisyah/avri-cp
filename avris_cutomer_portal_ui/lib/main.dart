import 'package:avris_cutomer_portal_ui/core/utils/custom_scroll_behavior.dart';
import 'package:avris_cutomer_portal_ui/modules/login/view/login_screen.dart';
import 'package:avris_cutomer_portal_ui/base/menuController.dart';
import 'package:avris_cutomer_portal_ui/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'core/thema/app_theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ControllerMenu(),
      child: Consumer<ControllerMenu>(
          builder: (context, localeModel, child) => GetMaterialApp(
                debugShowCheckedModeBanner: false,
                // key: ControllerMenu().scaffoldKey,
                title: 'Customer Portal',
                theme: ThemeClass.lightTheme,
                scrollBehavior: CustomScrollBehavior(),
                localizationsDelegates: AppLocalizations.localizationsDelegates,
                supportedLocales: AppLocalizations.supportedLocales,
                locale: localeModel.currentLocale,
                home: const LoginScreen(),
                getPages: AppPages.routes,
                defaultTransition: Transition
                    .noTransition, // disable page transition animation
                transitionDuration: const Duration(seconds: 0),
              )),
    );
  }
}
