import 'package:avris_cutomer_portal_ui/core/value/constant.dart' as constant;
import 'package:avris_cutomer_portal_ui/core/value/constant.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ThemeClass {
  static ThemeData lightTheme = ThemeData(
    primaryColor: kPrimaryColor,
    scaffoldBackgroundColor: Colors.white,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        elevation: 8,
        primary: Colors.transparent,
        maximumSize: const Size(double.maxFinite, 56),
        minimumSize: const Size(double.maxFinite, 56),
      ),
    ),
    inputDecorationTheme: const InputDecorationTheme(
      filled: false,
      // fillColor: kPrimaryLightColor,
      // iconColor: kPrimaryColor,
      // prefixIconColor: kPrimaryColor,
      contentPadding: EdgeInsets.symmetric(
          horizontal: defaultPadding, vertical: defaultPadding),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(30)),
        borderSide: BorderSide.none,
      ),
    ),
    dataTableTheme: DataTableThemeData(
        headingTextStyle:
            GoogleFonts.lato(fontWeight: FontWeight.bold, color: Colors.black)
        // TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontStyle: ),
        ),
  );

  static ThemeData darkTheme = ThemeData(
    // scaffoldBackgroundColor: Colors.black,
    // colorScheme: const ColorScheme.dark(),
    appBarTheme: const AppBarTheme(
      backgroundColor: constant.PRIMARY_BLUE,
    ),
    buttonTheme: const ButtonThemeData(buttonColor: Colors.white),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        textStyle: GoogleFonts.lato(),
        padding: EdgeInsets.zero,
        foregroundColor: constant.PRIMARY_BLUE,
      ),
    ),
    textTheme: GoogleFonts.latoTextTheme(ThemeData.light().textTheme),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        textStyle: MaterialStateProperty.all(GoogleFonts.lato()),
        backgroundColor: MaterialStateProperty.all(constant.PRIMARY_BLUE),
      ),
    ),
    dataTableTheme: DataTableThemeData(
        headingTextStyle:
            GoogleFonts.lato(fontWeight: FontWeight.bold, color: Colors.black)
        // TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontStyle: ),
        ),
    // primarySwatch: Colors.grey,
    primaryColor: constant.PRIMARY_BLUE,
    // brightness: Brightness.dark,
    backgroundColor: constant.PRIMARY_BLUE,
    // dividerColor: Colors.black12,
  );
}
