// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

//COLOR
// const Color PRIMARY_BLUE = Color(0xFF005AAA);
const Color PRIMARY_BLUE = Color.fromRGBO(22, 87, 151, 1);
const Color ERROR_COLOR = Colors.red;
const kPrimaryColor = Color.fromRGBO(89, 45, 131, 1);
const kPrimaryLightColor = Color.fromRGBO(162, 112, 226, 1);

const double defaultPadding = 16.0;
