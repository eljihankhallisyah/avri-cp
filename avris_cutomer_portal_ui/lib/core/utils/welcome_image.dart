import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WelcomeImage extends StatelessWidget {
  const WelcomeImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(25, 15, 0, 15),
            height: MediaQuery.of(context).size.height * 0.9,
            width: 650,
            padding: EdgeInsets.all(60),
            decoration: BoxDecoration(
              color: Color.fromRGBO(249, 249, 249, 1),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(
              child: Expanded(
                flex: 8,
                child: SvgPicture.asset(
                  "assets/icons/Img_indiv (1).svg",
                ),
              ),
            ))
      ],
    );
  }
}
