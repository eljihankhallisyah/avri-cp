class AppException implements Exception {
  final String message;
  final int errorCode;
  AppException({required this.message, required this.errorCode});
}

class FetchDataException implements Exception {
  final String message;
  FetchDataException(this.message);
}

class AuthException implements Exception {
  final String message;
  final int errorCode;
  AuthException({required this.message, required this.errorCode});
}
