import 'package:avris_cutomer_portal_ui/core/utils/home_icon_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sidebarx/sidebarx.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SidebarMenu extends StatelessWidget {
  const SidebarMenu({
    Key? key,
    required SidebarXController controller,
  })  : _controller = controller,
        super(key: key);

  final SidebarXController _controller;

  @override
  Widget build(BuildContext context) {
    var t = AppLocalizations.of(context);
    return SidebarX(
      controller: _controller,
      theme: SidebarXTheme(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.28),
                blurRadius: 30,
              )
            ]),
        textStyle: const TextStyle(color: Colors.black),
        selectedTextStyle: const TextStyle(color: Colors.white),
        itemTextPadding: const EdgeInsets.only(left: 30),
        selectedItemTextPadding: const EdgeInsets.only(left: 30),
        itemDecoration: BoxDecoration(
          border: Border.all(color: Colors.white),
        ),
        selectedItemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Color.fromARGB(255, 186, 148, 222),
          ),
          gradient: const LinearGradient(
              colors: [Color(0xFF592D83), Color(0xFFA270E2)]),
        ),
        iconTheme: const IconThemeData(
          color: Color.fromRGBO(190, 191, 207, 1),
          size: 20,
        ),
        selectedIconTheme: const IconThemeData(color: Colors.white),
      ),
      extendedTheme: const SidebarXTheme(
        width: 250,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        margin: EdgeInsets.only(right: 10),
      ),
      headerBuilder: (context, extended) {
        return SizedBox(
          height: 90,
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Image.asset('assets/images/Logo_Avrist_Ungu.png'),
          ),
        );
      },
      items: const [
        SidebarXItem(
          icon: HomeIcon.home,
          label: 'Beranda',
        ),
        SidebarXItem(
          icon: HomeIcon.vector,
          label: 'Asuransi Saya',
        ),
        SidebarXItem(
          icon: HomeIcon.doct,
          label: 'Formulir',
        ),
        SidebarXItem(
          icon: HomeIcon.card,
          label: 'E-payment',
        ),
        SidebarXItem(
          icon: HomeIcon.doc_shield,
          label: 'Klaim Saya',
        ),
        SidebarXItem(
          icon: HomeIcon.logo_icon,
          label: 'Web Avrist',
        ),
      ],
    );
  }
}
