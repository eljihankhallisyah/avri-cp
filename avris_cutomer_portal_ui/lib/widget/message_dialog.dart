import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MessageDialog extends StatelessWidget {
  final String? title;
  final String? content;
  final void Function()? onPressed;
  const MessageDialog({Key? key, this.title, this.content, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: title != null ? Text(title!) : null,
      content: content != null ? Text(content!) : null,
      actions: [
        // ElevatedButton(
        //   style: ElevatedButton.styleFrom(
        //     primary: Colors.blue, // Ganti dengan warna yang diinginkan
        //   ),
        //   onPressed: onPressed ?? () => Get.back(),
        //   child: const Text("Ok"),

        // )

        Container(
          margin: const EdgeInsets.fromLTRB(15, 0, 15, 0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(89, 45, 131, 1),
                Color.fromRGBO(162, 112, 226, 1),
              ], // set the colors for the gradient
              begin: Alignment
                  .bottomCenter, // set the starting position of the gradient
              end: Alignment
                  .topCenter, // set the ending position of the gradient
            ),
            borderRadius: BorderRadius.circular(10), // set border radius
          ),
          child: Center(
            child: ElevatedButton(
              onPressed: onPressed ?? () => Get.back(),
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
