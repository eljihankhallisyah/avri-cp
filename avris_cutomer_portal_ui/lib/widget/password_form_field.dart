import 'package:avris_cutomer_portal_ui/widget/custom_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PasswordFormField extends StatefulWidget {
  final dynamic initialValue;
  final FormFieldValidator<String>? validator;
  final List<TextInputFormatter>? inputFormatters;
  final String name;
  final String label;
  final bool readOnly;
  final bool isDense;
  final EdgeInsetsGeometry? contentPadding;
  final TextEditingController? controller;
  final bool withLabel;
  final InputDecoration? inputDecoration;
  const PasswordFormField(
      {Key? key,
      this.initialValue,
      this.withLabel = false,
      this.validator,
      this.controller,
      this.inputFormatters,
      this.isDense = false,
      this.contentPadding,
      required this.name,
      required this.label,
      this.readOnly = false,
      this.inputDecoration})
      : super(key: key);

  @override
  State<PasswordFormField> createState() => _PasswordFormFieldState();
}

class _PasswordFormFieldState extends State<PasswordFormField> {
  var obsecure = true;

  void showHide() {
    setState(() {
      obsecure = !obsecure;
    });
  }

  @override
  Widget build(BuildContext context) {
    var child = FormBuilderTextField(
      name: widget.name,
      controller: widget.controller,
      initialValue: widget.initialValue,
      validator: widget.validator,
      readOnly: widget.readOnly,
      inputFormatters: widget.inputFormatters,
      obscureText: obsecure,
      decoration: widget.inputDecoration ??
          InputDecoration(
            suffixIcon: IconButton(
              icon: Icon(
                obsecure ? Icons.visibility : Icons.visibility_off,
              ),
              onPressed: showHide,
            ),
            prefixIcon: const Icon(Icons.lock),
            contentPadding: widget.contentPadding,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            isDense: widget.isDense,
            hintText: widget.label,
          ),
    );
    return widget.withLabel ? CustomFormField(child) : child;
  }
}
