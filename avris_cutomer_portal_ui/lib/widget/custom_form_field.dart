// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:avris_cutomer_portal_ui/core/utils/responsive.dart';
import 'package:flutter/material.dart';

class CustomFormField extends StatelessWidget {
  final field;
  final bool typeaheadfield;
  // ignore: use_key_in_widget_constructors
  const CustomFormField(this.field, [this.typeaheadfield = false]);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Column(children: [
      Wrap(
          alignment: WrapAlignment.spaceBetween,
          crossAxisAlignment: WrapCrossAlignment.center,
          runSpacing: 10.0,
          children: [
            SizedBox(
                width: Responsive.isMobile(context)
                    ? mediaQuery.size.width
                    : mediaQuery.size.width * 0.22,
                child: SelectableText(
                  typeaheadfield
                      ? field.textFieldConfiguration.decoration.hintText
                          .toString()
                      : field.decoration.hintText.toString(),
                  style: const TextStyle(fontWeight: FontWeight.bold),
                )),
            SizedBox(
                width: Responsive.isMobile(context)
                    ? mediaQuery.size.width
                    : mediaQuery.size.width * 0.48,
                child: field),
          ]),
      const SizedBox(height: 10)
    ]);
  }
}
