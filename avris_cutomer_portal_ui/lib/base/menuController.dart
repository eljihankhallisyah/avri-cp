import 'package:flutter/material.dart';

class ControllerMenu extends ChangeNotifier {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
  Locale? _locale;

  Locale? get locale => _locale;

  void set(Locale locale) {
    _locale = locale;
    notifyListeners();
  }

  void controlMenu() {
    if (!_scaffoldKey.currentState!.isDrawerOpen) {
      _scaffoldKey.currentState!.openDrawer();
    }
  }

  Locale? _currentLocale = const Locale("id");
  String _currentLang = "id";
  Locale? get currentLocale => _currentLocale;
  String get currentLang {
    var lang;
    switch (_currentLang) {
      case "id":
        lang = "Bahasa";
        break;
      case "en":
        lang = "English";
        break;
      default:
        lang = "Bahasa";
    }
    return lang;
  }
}
