import 'package:avris_cutomer_portal_ui/modules/captcha/controller/captcha_controller.dart';
import 'package:avris_cutomer_portal_ui/modules/captcha/view/captcha_screen.dart';
import 'package:avris_cutomer_portal_ui/modules/dasboard/controller/dasboard_controller.dart';
import 'package:avris_cutomer_portal_ui/modules/dasboard/view/screen_beranda.dart';
import 'package:avris_cutomer_portal_ui/modules/otp/controller/otp_controller.dart';
import 'package:avris_cutomer_portal_ui/modules/otp/view/otp_form.dart';
import 'package:get/get.dart';

import 'package:avris_cutomer_portal_ui/modules/login/controller/login_controller.dart';
import 'package:avris_cutomer_portal_ui/modules/login/view/login_screen.dart';
import 'package:avris_cutomer_portal_ui/modules/register/controller/register_controller.dart';
import 'package:avris_cutomer_portal_ui/modules/register/view/register_form.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();
  static final routes = [
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginScreen(),
      binding: BindingsBuilder(() {
        Get.put(LoginController());
      }),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => const RegisterForm(),
      binding: BindingsBuilder(() {
        Get.put(RegisterController());
      }),
    ),
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => BerandaScreen(),
      binding: BindingsBuilder(() {
        Get.put(DasboardController());
      }),
    ),
    GetPage(
      name: _Paths.OTP,
      page: () => OtpForm(),
      binding: BindingsBuilder(() {
        Get.put(OtpController());
      }),
    ),
    GetPage(
      name: _Paths.CAPTCHA,
      page: () => CaptchaScreen(),
      binding: BindingsBuilder(() {
        Get.put(CaptchaController());
      }),
    ),
  ];
}
