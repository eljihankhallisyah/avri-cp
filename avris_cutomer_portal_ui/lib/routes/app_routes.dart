part of 'app_pages.dart';

abstract class Routes {
  Routes._();

  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const OTP = _Paths.OTP;
  static const CAPTCHA = _Paths.CAPTCHA;
}

abstract class _Paths {
  _Paths._();

  static const LOGIN = LoginScreen.routeName;
  static const REGISTER = RegisterForm.routeName;
  static const DASHBOARD = BerandaScreen.routeName;
  static const OTP = OtpForm.routeName;
  static const CAPTCHA = CaptchaScreen.routeName;
}
