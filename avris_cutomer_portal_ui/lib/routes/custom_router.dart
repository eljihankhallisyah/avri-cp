import 'package:avris_cutomer_portal_ui/modules/captcha/view/captcha_screen.dart';
import 'package:avris_cutomer_portal_ui/modules/dasboard/view/screen_beranda.dart';
import 'package:avris_cutomer_portal_ui/modules/otp/view/otp_form.dart';

import 'package:avris_cutomer_portal_ui/modules/register/view/register_form.dart';

class CustomRouter {
  static final route = {
    // '/login-gagal': (ctx) => FailedLogin(),
    '/captcha': (ctx) => CaptchaScreen(),
    // '/reset-password': (ctx) => ResetForm(),
    // '/link-success': (ctx) => LinkSuccess(),
    // '/ganti-password': (ctx) => NewPassword(),
    // '/ganti-password-berhasil': (ctx) => ChangeSucess(),
    // '/daftar': (ctx) => RegisterForm(),
    // '/otp': (ctx) => OtpForm(),
    // '/otp-gagal': (ctx) => OTPFailed(),
    // '/berhasil-mendaftar': (ctx) => RegisterSuccess(),
    '/beranda': (ctx) => BerandaScreen(),
  };
}
