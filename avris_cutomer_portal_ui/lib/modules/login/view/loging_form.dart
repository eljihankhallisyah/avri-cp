import 'package:avris_cutomer_portal_ui/core/utils/logger.dart';
import 'package:avris_cutomer_portal_ui/modules/captcha/view/captcha_screen.dart';
import 'package:avris_cutomer_portal_ui/modules/dasboard/view/screen_beranda.dart';
import 'package:avris_cutomer_portal_ui/modules/login/controller/login_controller.dart';
import 'package:avris_cutomer_portal_ui/modules/register/view/register_form.dart';

import 'package:avris_cutomer_portal_ui/widget/message_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  void initState() {
    // loadSavedData();
    super.initState();
  }

  final LoginController controller = Get.put(LoginController());
  final _formKey = GlobalKey<FormBuilderState>();
  final AppLocalizations appLoc = AppLocalizations.of(Get.context!);
  bool _passwordVisible = false;

  void submit() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      final formData = _formKey.currentState!.value;
      logger.i(formData);
      final result = await controller.login(formData);
      print(formData);
      if (result == true) {
        // controller.checkExpired();
        Get.offNamed(BerandaScreen.routeName);
        // Get.offNamed(CaptchaScreen.routeName);
      } else {
        Get.dialog(
          MessageDialog(
            title: appLoc.loginFailed,
            content: appLoc.loginFailedMessage,
            onPressed: () {
              Get.back();
            },
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final appLoc = AppLocalizations.of(context);
    return Center(
      child: GetBuilder<LoginController>(
        builder: (controller) {
          return Stack(
            children: <Widget>[
              Container(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.home,
                          size: 25,
                        ),
                        SizedBox(width: 10),
                        Text('Masuk',
                            style: TextStyle(
                              fontWeight: FontWeight
                                  .bold, // Set the font weight to bold
                              fontSize: 20, // Set the font size to 24 pixels
                            )),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.all(40),
                    ),
                    Table(children: [
                      TableRow(children: [
                        FormBuilder(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Color.fromRGBO(249, 249, 249, 1),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                padding: EdgeInsets.all(16),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      'assets/icons/Frame 13474.svg',
                                      height: 50,
                                      width: 50,
                                    ),
                                    SizedBox(width: 16),
                                    Text(
                                      'Avrist Individu',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(1),
                              ),
                              SizedBox(width: 34),
                              Container(
                                child: Text("Login ke customer Dashboard"),
                                margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                                padding: EdgeInsets.only(bottom: 10),
                              ),
                              Padding(
                                padding: EdgeInsets.all(1),
                              ),
                              SizedBox(width: 34),
                              Container(
                                width: 58,
                                alignment: Alignment.center,
                                color: Colors.white,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: ListTile(
                                        subtitle: FormBuilderTextField(
                                            name: "username",
                                            initialValue:
                                                controller.savedUserName,
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(
                                                  errorText:
                                                      appLoc.requiredErrorText),
                                            ]),
                                            decoration: const InputDecoration(
                                              filled: true,
                                              fillColor: Colors.white,
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(8))),
                                              hintText: 'Masukan Email',
                                              labelText: 'Email',
                                              isDense: true,
                                            ),
                                            onChanged: (value) {
                                              controller.username.value =
                                                  value!;
                                            }),
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    //NIK

                                    //nama
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 14),
                              ),
                              Container(
                                  child: Row(
                                children: [
                                  //Agent Code

                                  Expanded(
                                    child: ListTile(
                                        subtitle: FormBuilderTextField(
                                      name: 'password',
                                      obscureText:
                                          !_passwordVisible, // Set obscureText based on password visibility
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        labelText: appLoc.password,
                                        hintText: 'Masukan Password',
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            _passwordVisible
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            size: 20,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              _passwordVisible =
                                                  !_passwordVisible;
                                            });
                                          },
                                        ),
                                      ),
                                      onChanged: (value) {
                                        controller.password.value = value!;
                                      },
                                    )),
                                  ),
                                  SizedBox(width: 10),

                                  //Dist Channel
                                ],
                              )),
                              Container(
                                  child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  // Your other login form fields and buttons here

                                  Expanded(
                                    child: Row(children: [
                                      Spacer(),
                                      Text("Lupa password?"),
                                      TextButton(
                                          onPressed: () {
                                            // Navigator.pushNamed(
                                            //     context, ResetForm.routeName);
                                          },
                                          child: Container(
                                            width: 55,
                                            child: Text(
                                              'Reset',
                                              style: TextStyle(
                                                foreground: Paint()
                                                  ..shader = LinearGradient(
                                                    colors: [
                                                      Color(0xFF592D83),
                                                      Color(0xFFA270E2)
                                                    ],
                                                    begin: Alignment.topLeft,
                                                    end: Alignment.bottomRight,
                                                  ).createShader(Rect.fromLTWH(
                                                      0, 0, 200, 70)),
                                              ),
                                            ),
                                          )),
                                    ]),
                                  ),
                                ],
                              )),
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 15),
                              ),
                              Container(
                                margin: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromRGBO(89, 45, 131, 1),
                                      Color.fromRGBO(162, 112, 226, 1),
                                    ], // set the colors for the gradient
                                    begin: Alignment
                                        .bottomCenter, // set the starting position of the gradient
                                    end: Alignment
                                        .topCenter, // set the ending position of the gradient
                                  ),
                                  borderRadius: BorderRadius.circular(
                                      10), // set border radius
                                ),
                                child: Center(
                                  child: ElevatedButton(
                                    onPressed: submit,
                                    child: Text(
                                      'Masuk',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 10),
                              Container(
                                width: 200,
                                height: 50,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("Belum memiliki akun?"),
                                    TextButton(
                                        onPressed: () {
                                          Get.offNamed(RegisterForm.routeName);
                                        },
                                        child: Container(
                                          width: 55,
                                          child: Text(
                                            'Daftar',
                                            style: TextStyle(
                                              foreground: Paint()
                                                ..shader = LinearGradient(
                                                  colors: [
                                                    Color(0xFF592D83),
                                                    Color(0xFFA270E2)
                                                  ],
                                                  begin: Alignment.topLeft,
                                                  end: Alignment.bottomRight,
                                                ).createShader(Rect.fromLTWH(
                                                    0, 0, 200, 70)),
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]),
                    ]),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                padding: EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(color: Colors.white),
              ),
            ],
          );
        },
      ),
    );
  }
}
