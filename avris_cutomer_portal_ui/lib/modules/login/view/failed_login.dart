import 'package:avris_cutomer_portal_ui/core/utils/background.dart';
import 'package:avris_cutomer_portal_ui/core/utils/responsive.dart';
import 'package:avris_cutomer_portal_ui/modules/login/view/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class FailedLogin extends StatelessWidget {
  const FailedLogin({Key? key}) : super(key: key);
  static const routeName = "/login-gagal";

  @override
  Widget build(BuildContext context) {
    return Background(
        child: SingleChildScrollView(
      child: Responsive(
          mobile: const MobileFailedLogin(),
          desktop: Row(
            children: [
              Column(children: [
                Container(
                  margin: EdgeInsets.all(15),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width * 0.65,
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(249, 249, 249, 1),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                    child: Expanded(
                      child: SvgPicture.asset(
                        "assets/icons/Frame_640.svg",
                      ),
                    ),
                  ),
                ),
              ]),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width * 0.35,
                        margin: EdgeInsets.all(30),
                        // child: InputPassword(),
                        child: Stack(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  'Tidak bisa login 1x24 jam',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                              ],
                            ),
                            Center(child: const FailedDescription())
                          ],
                        ))
                  ],
                ),
              )
            ],
          )),
    ));
  }
}

class MobileFailedLogin extends StatelessWidget {
  const MobileFailedLogin({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 8,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              height: MediaQuery.of(context).size.height,
              margin: EdgeInsets.all(30),
              child: Stack(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Tidak bisa login 1x24 jam',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  Center(
                      child: Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              width: MediaQuery.of(context).size.width,
                              height: 350,
                              child: SvgPicture.asset(
                                "assets/icons/Frame_640.svg",
                              )),
                          const FailedDescription()
                        ]),
                  ))
                ],
              ))
        ],
      ),
    );
  }
}

class FailedDescription extends StatelessWidget {
  const FailedDescription({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Container(
        margin: EdgeInsets.fromLTRB(30, 0, 30, 20),
        child: Text(
          'Kami mendeteksi kamu telah salah 3x, untuk keamanan silahkan isi captcha diatas',
          style: TextStyle(fontSize: 15),
          textAlign: TextAlign.center,
        ),
      ),
      Container(
        margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
        // width: 400,
        decoration: BoxDecoration(
          gradient: const LinearGradient(
            colors: [
              Color.fromRGBO(89, 45, 131, 1),
              Color.fromRGBO(162, 112, 226, 1),
            ], // set the colors for the gradient
            begin: Alignment
                .bottomCenter, // set the starting position of the gradient
            end: Alignment.topCenter, // set the ending position of the gradient
          ),
          borderRadius: BorderRadius.circular(6), // set border radius
        ),
        child: ElevatedButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const LoginScreen();
              }));
            },
            child: const Text('Oke',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ))),
      )
    ]);
  }
}
