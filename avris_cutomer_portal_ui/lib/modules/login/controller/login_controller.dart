import 'package:avris_cutomer_portal_ui/core/utils/logger.dart';
import 'package:avris_cutomer_portal_ui/data/provider/auth_provider.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginController extends GetxController {
  // Initialize GetStorage instance
  var username = ''.obs; // Username variable
  var password = ''.obs;
  var isLoading = false.obs;
  var savedUserName = "";
  final provider = Get.put(AuthCustomerPortalProvider());

  @override
  void onInit() {
    super.onInit();
    checkPermission();
    // getSavedUserNameFromPref();
  }

  Future<void> logout() async {
    await provider.logout();
    getSavedUserNameFromPref();
  }

  void checkPermission() async {
    try {
      var status = await Permission.storage.status;
      if (!status.isGranted) {
        await Permission.storage.request();
      }
    } catch (e) {
      logger.d(e.toString());
    }
  }

  void getSavedUserNameFromPref() async {
    var returnValue = await Future.wait([getSavedUserName()]);
    var s = returnValue[0];
    logger.d("savedUserName : $s");
  }

  void checkExpired() async {
    final prefs = await SharedPreferences.getInstance();
    var expiredtime =
        DateTime.parse(prefs.getString("expired")!).millisecondsSinceEpoch ~/
            1000;
    Future.delayed(Duration(milliseconds: expiredtime), () {
      logout();
    });
  }

  Future<String> getSavedUserName() async {
    await SharedPreferences.getInstance()
        .then((prefs) => savedUserName = prefs.getString('username') ?? "");
    // final prefs = await SharedPreferences.getInstance();
    // Future.delayed(const Duration(seconds: 1), () {
    //   savedUserName = prefs.getString('username') ?? "";
    //   logger.d("savedUserName : $savedUserName");
    // });
    return savedUserName;
  }

  Future<bool> login(Map<String, dynamic> formData) async {
    isLoading(true);
    final result = await provider.login(formData);
    return result;
  }
}
