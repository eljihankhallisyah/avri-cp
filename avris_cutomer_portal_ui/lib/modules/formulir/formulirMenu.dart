import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class FormulirMenu extends StatefulWidget {
  FormulirMenu({Key? key}) : super(key: key);
  @override
  State<FormulirMenu> createState() => _FormulirMenuState();
}

class _FormulirMenuState extends State<FormulirMenu> {
  int _selectedIndex = 0;

  void setIndex(index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(30, 5, 30, 5),
            child: const Row(
              children: [
                Icon(Icons.arrow_back),
                Padding(padding: EdgeInsets.only(left: 10)),
                Text('Formulir',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))
              ],
            ),
          ),
          SizedBox(height: 10),
          Center(
            child: Container(
              constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.75),
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(7)),
              child: Column(
                children: [
                  if (_selectedIndex == 0)
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(89, 45, 131, 1),
                                Color.fromRGBO(162, 112, 226, 1),
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: MaterialButton(
                              onPressed: () {},
                              child: Text(
                                'POS',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 13),
                              ),
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(right: 10)),
                        Container(
                          child: Center(
                            child: MaterialButton(
                              onPressed: () {
                                setIndex(1);
                              },
                              child: Text(
                                'Klaim',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  if (_selectedIndex == 1)
                    Row(
                      children: [
                        Container(
                          child: Center(
                            child: MaterialButton(
                              onPressed: () {
                                setIndex(0);
                              },
                              child: Text(
                                'POS',
                              ),
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(right: 10)),
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(89, 45, 131, 1),
                                Color.fromRGBO(162, 112, 226, 1),
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: MaterialButton(
                              onPressed: () {},
                              child: Text(
                                'Klaim',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 13),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Divider(),
                  ),
                  for (var i = 0; i < 3; i++)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        for (var i = 0; i < 4; i++)
                          Container(
                            constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.17),
                            padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                            // padding: EdgeInsets.all(15),
                            margin: EdgeInsets.symmetric(
                                horizontal: 5, vertical: 8),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey.shade300),
                                borderRadius: BorderRadius.circular(7)),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(right: 5),
                                      child: Icon(Icons.description_outlined,
                                          size: 18,
                                          color:
                                              Color.fromRGBO(115, 103, 240, 1)),
                                    ),
                                    Text('Nama file / dokumen ...',
                                        style: TextStyle(
                                          fontSize: 14,
                                        )),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8),
                                  child: MaterialButton(
                                      onPressed: () => _launchURL(
                                          "https://avrist.com/Formulir%20Klaim%20Rawat%20Jalan%20dan%20Pembedahan%20Gigi.pdf"),
                                      hoverColor: Colors.transparent,
                                      focusColor: Colors.transparent,
                                      child: Container(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 8),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            border: Border.all(
                                                color: Color.fromRGBO(
                                                    162, 112, 226, 1))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 2),
                                              child: Icon(
                                                  Icons.file_download_outlined,
                                                  size: 15,
                                                  color: Color.fromRGBO(
                                                      162, 112, 226, 1)),
                                            ),
                                            Text('Download',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Color.fromRGBO(
                                                        162, 112, 226, 1))),
                                          ],
                                        ),
                                      )),
                                )
                              ],
                            ),
                          )
                      ],
                    )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
