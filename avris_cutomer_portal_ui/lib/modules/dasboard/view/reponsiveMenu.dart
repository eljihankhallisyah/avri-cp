import 'package:avris_cutomer_portal_ui/base/menuController.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'notifikasi.dart';

class ReponsiveMenu extends StatelessWidget {
  const ReponsiveMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color.fromARGB(255, 242, 242, 242),
        child: Container(
            margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
            height: 50,
            padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.10),
                    blurRadius: 10,
                  )
                ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    IconButton(
                        onPressed: context.read<ControllerMenu>().controlMenu,
                        icon: Icon(Icons.menu)),
                    Container(
                      margin: EdgeInsets.only(left: 5),
                      child: Notifikasi(),
                    )
                  ],
                ),
                SizedBox(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Image.asset('assets/images/Logo_Avrist_Ungu.png'),
                  ),
                )
              ],
            )));
  }
}
