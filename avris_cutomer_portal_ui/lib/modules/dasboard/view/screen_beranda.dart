import 'package:avris_cutomer_portal_ui/core/utils/home_icon_icons.dart';
import 'package:avris_cutomer_portal_ui/core/utils/logger.dart';
import 'package:avris_cutomer_portal_ui/core/utils/responsive.dart';
import 'package:avris_cutomer_portal_ui/modules/dasboard/view/helloUser.dart';
import 'package:avris_cutomer_portal_ui/modules/dasboard/view/notificationBarDesktop.dart';
import 'package:avris_cutomer_portal_ui/modules/dasboard/view/profile.dart';
import 'package:avris_cutomer_portal_ui/modules/formulir/formulirMenu.dart';
import 'package:avris_cutomer_portal_ui/modules/login/view/login_screen.dart';
import 'package:avris_cutomer_portal_ui/modules/polis_saya/view/polisDetail.dart';
import 'package:avris_cutomer_portal_ui/modules/polis_saya/view/polisMenu.dart';
import 'package:avris_cutomer_portal_ui/base/menuController.dart';
import 'package:avris_cutomer_portal_ui/widget/sidebar_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidebarx/sidebarx.dart';

import 'reponsiveMenu.dart';

/**
 * Main component
 */ ///
class BerandaScreen extends StatefulWidget {
  BerandaScreen({Key? key}) : super(key: key);
  static const routeName = "/beranda";
  String? stringValue;
  @override
  State<BerandaScreen> createState() => _BerandaState();
}

class _BerandaState extends State<BerandaScreen> {
  var _controller = SidebarXController(selectedIndex: 0, extended: true);
  int _selectedIndex = 1;

  void setIndex(index) {
    setState(() {
      _controller = SidebarXController(selectedIndex: index, extended: true);
    });
  }

  Future<void> data() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    widget.stringValue = prefs.getString("username");
  }

  @override
  void initState() {
    super.initState();
    data();
    setIndex(6);
  }

  @override
  Widget build(BuildContext context) {
    final user = widget.stringValue;
    return Scaffold(
        drawer: SidebarMenu(controller: _controller),
        key: context.read<ControllerMenu>().scaffoldKey,
        body: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (Responsive.isDesktop(context))
              SidebarMenu(controller: _controller),
            Expanded(
              child: Container(
                color: Color.fromARGB(255, 242, 242, 242),
                child: Column(
                  children: [
                    // Notification bar
                    if (!Responsive.isDesktop(context)) const ReponsiveMenu(),
                    if (Responsive.isDesktop(context))
                      NotificationBarDesktop(callback: setIndex),
                    Expanded(
                      child: RouterScreen(controller: _controller),
                    ),
                    if (!Responsive.isDesktop(context))
                      BottomNavigationBar(
                        currentIndex: _selectedIndex,
                        selectedItemColor: Color(0xFFA270E2),
                        items: [
                          BottomNavigationBarItem(
                              icon: Container(
                                  padding: EdgeInsets.only(top: 3),
                                  child: Icon(HomeIcon.vector)),
                              label: ''),
                          BottomNavigationBarItem(
                              icon: Icon(Icons.home), label: ''),
                          BottomNavigationBarItem(
                              icon: Container(
                                padding: EdgeInsets.only(top: 10),
                                child: CircleAvatar(
                                  backgroundColor: Colors.transparent,
                                  child: SvgPicture.asset(
                                    'assets/icons/Frame_650.svg',
                                    width: 25,
                                    height: 20,
                                  ),
                                ),
                              ),
                              label: '')
                        ],
                        onTap: (int index) async {
                          this._selectedIndex = index;
                          if (index == 0) {
                            setIndex(1);
                          } else if (index == 2) {
                            await showMenu<String>(
                              context: context,
                              position:
                                  RelativeRect.fromLTRB(385.0, 385.0, 0.0, 0.0),
                              items: <PopupMenuItem<String>>[
                                PopupMenuItem<String>(
                                    child: Container(
                                        width: 175,
                                        margin:
                                            EdgeInsets.fromLTRB(20, 20, 20, 10),
                                        padding: EdgeInsets.only(bottom: 10),
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                              color: Colors.black26,
                                              width: 1.0,
                                            ),
                                          ),
                                        ),
                                        child: SizedBox(
                                            width: 175,
                                            child: Flexible(
                                                child: Column(children: [
                                              Row(
                                                children: [
                                                  CircleAvatar(
                                                    backgroundColor:
                                                        Colors.transparent,
                                                    child: SvgPicture.asset(
                                                      'assets/icons/Frame_650.svg',
                                                      width: 40,
                                                      height: 40,
                                                    ),
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 20)),
                                                  Text(user.toString(),
                                                      softWrap: true,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12)),
                                                ],
                                              ),
                                            ]))))),
                                PopupMenuItem<String>(
                                  onTap: () {
                                    setIndex(6);
                                  },
                                  child: Container(
                                      width: 180,
                                      height: 25,
                                      margin: EdgeInsets.fromLTRB(15, 5, 5, 0),
                                      padding: EdgeInsets.only(bottom: 3),
                                      child: SizedBox(
                                        width: 175,
                                        child: Row(
                                          children: [
                                            CircleAvatar(
                                              backgroundColor:
                                                  Colors.transparent,
                                              child: SvgPicture.asset(
                                                'assets/icons/Frame_650.svg',
                                                width: 90,
                                                height: 90,
                                              ),
                                            ),
                                            const Text('Profil',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 13)),
                                          ],
                                        ),
                                      )),
                                ),
                                PopupMenuItem<String>(
                                  onTap: () {
                                    setIndex(7);
                                  },
                                  child: Container(
                                      width: 180,
                                      height: 25,
                                      margin: EdgeInsets.fromLTRB(15, 5, 5, 0),
                                      padding: EdgeInsets.only(bottom: 3),
                                      child: SizedBox(
                                        width: 175,
                                        child: Row(
                                          children: [
                                            Icon(Icons.vpn_key_outlined,
                                                size: 20,
                                                color: Color.fromRGBO(
                                                    190, 191, 207, 1)),
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(left: 12)),
                                            Text('Ubah password',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 13)),
                                          ],
                                        ),
                                      )),
                                ),
                                new PopupMenuItem<String>(
                                  onTap: () {
                                    setIndex(7);
                                  },
                                  child: Container(
                                      width: 180,
                                      height: 25,
                                      margin: EdgeInsets.fromLTRB(15, 5, 5, 0),
                                      padding: EdgeInsets.only(bottom: 3),
                                      child: SizedBox(
                                        width: 175,
                                        child: Row(
                                          children: [
                                            Icon(Icons.exit_to_app_outlined,
                                                size: 20,
                                                color: Color.fromRGBO(
                                                    190, 191, 207, 1)),
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(left: 12)),
                                            Text('Keluar',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 13)),
                                          ],
                                        ),
                                      )),
                                )
                              ],
                              elevation: 8.0,
                            );
                          } else {
                            setIndex(0);
                          }
                        },
                      )
                  ],
                ),
              ),
            )
          ],
        ));
  }
}

/**
 * Router 
 */ ///
class RouterScreen extends StatefulWidget {
  const RouterScreen({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final SidebarXController controller;

  @override
  State<RouterScreen> createState() => _RouterState();
}

class _RouterState extends State<RouterScreen> {
  var polisState = 0;

  void setPolisState(index) {
    setState(() {
      polisState = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return AnimatedBuilder(
      animation: widget.controller,
      builder: (context, child) {
        switch (widget.controller.selectedIndex) {
          case 0:
            if (Responsive.isDesktop(context)) {
              return const DesktopBerandaComponent();
            } else {
              return const MobileBerandaComponent();
            }
          case 1:
            if (Responsive.isDesktop(context)) {
              if (polisState == 0) {
                return PolisMenu(callback: setPolisState);
              } else {
                return PolisDetail(callback: setPolisState);
              }
            } else {
              if (polisState == 0) {
                return PolisMenuMobile(callback: setPolisState);
              } else {
                return Text('Polis Detail');
              }
            }
          case 2:
            if (Responsive.isDesktop(context)) {
              return FormulirMenu();
            } else {
              return Text('formulir');
            }
          case 3:
            return Text(
              'Favorites',
              style: theme.textTheme.headlineSmall,
            );
          case 4:
            return Text(
              'Profile',
              style: theme.textTheme.headlineSmall,
            );
          case 5:
            return Text(
              'Settings',
              style: theme.textTheme.headlineSmall,
            );
          case 6:
            return const ProfileComponent();
          case 7:
            return AlertDialog(
              title: Text('Selected Option'),
              content: Text('Yakin Keluar'),
              actions: <Widget>[
                TextButton(
                    child: Text('Yes'),
                    onPressed: () {
                      Get.offNamed(LoginScreen.routeName);
                    }),
                TextButton(
                  child: Text('no'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BerandaScreen()),
                    );
                  },
                ),
              ],
            );

          default:
            return Text(
              'Not found page',
              style: theme.textTheme.headlineSmall,
            );
        }
      },
    );
  }
}

/**
 * if desktop
 */ ///
class DesktopBerandaComponent extends StatelessWidget {
  const DesktopBerandaComponent({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        // Hello user
        HelloUser(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Polis saya menu
            Container(
              margin: EdgeInsets.fromLTRB(30, 10, 20, 10),
              width: MediaQuery.of(context).size.width * 0.28,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 5),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.28,
                    height: 165,
                    child: SvgPicture.asset(
                      'assets/icons/Frame_13500.svg',
                      fit: BoxFit.fill,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Row(
                        children: [
                          Icon(
                            HomeIcon.vector,
                            size: 18,
                            color: Color.fromRGBO(110, 35, 206, 1),
                          ),
                          SizedBox(width: 10),
                          Text('Asuransi Saya',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12)),
                        ],
                      ),
                      IconButton(
                          onPressed: () {}, icon: Icon(Icons.arrow_right))
                    ],
                  )
                ],
              ),
            ),
            // Formulir menu
            Container(
              margin: EdgeInsets.fromLTRB(30, 10, 20, 10),
              width: MediaQuery.of(context).size.width * 0.28,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 5),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.28,
                    height: 165,
                    child: SvgPicture.asset(
                      'assets/icons/formulir_beranda.svg',
                      fit: BoxFit.fill,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Row(
                        children: [
                          Icon(HomeIcon.doct,
                              size: 18, color: Color.fromRGBO(68, 65, 251, 1)),
                          SizedBox(width: 10),
                          Text('Formulir',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12)),
                        ],
                      ),
                      IconButton(
                          onPressed: () {}, icon: Icon(Icons.arrow_right))
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Epayment menu
            Container(
              margin: EdgeInsets.fromLTRB(30, 0, 10, 10),
              width: MediaQuery.of(context).size.width * 0.20,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 5),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.25,
                    height: 165,
                    child: SvgPicture.asset(
                      'assets/icons/epayment_menu.svg',
                      fit: BoxFit.fill,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Row(
                        children: [
                          Icon(
                            HomeIcon.card,
                            size: 18,
                            color: Color.fromRGBO(231, 178, 80, 1),
                          ),
                          SizedBox(width: 10),
                          Text('E-payment',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12)),
                        ],
                      ),
                      IconButton(
                          onPressed: () {}, icon: Icon(Icons.arrow_right))
                    ],
                  )
                ],
              ),
            ),
            // Klaim menu
            Container(
              margin: EdgeInsets.fromLTRB(30, 0, 10, 10),
              width: MediaQuery.of(context).size.width * 0.20,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 5),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.25,
                    height: 165,
                    child: SvgPicture.asset(
                      'assets/icons/klaim_beranda.svg',
                      fit: BoxFit.fill,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Row(
                        children: [
                          Icon(
                            HomeIcon.doc_shield,
                            size: 18,
                            color: Color.fromRGBO(252, 131, 175, 1),
                          ),
                          SizedBox(width: 10),
                          Text('Klaim Saya',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12)),
                        ],
                      ),
                      IconButton(
                          onPressed: () {}, icon: Icon(Icons.arrow_right))
                    ],
                  )
                ],
              ),
            ),
            // Web avrist menu
            Container(
              margin: EdgeInsets.fromLTRB(30, 0, 10, 10),
              width: MediaQuery.of(context).size.width * 0.20,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 5),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.25,
                    height: 165,
                    child: SvgPicture.asset(
                      'assets/icons/web_menu.svg',
                      fit: BoxFit.fill,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Row(
                        children: [
                          Icon(
                            HomeIcon.logo_icon,
                            size: 18,
                            color: Color.fromRGBO(167, 175, 172, 1),
                          ),
                          SizedBox(width: 10),
                          Text('Web Avrist',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12)),
                        ],
                      ),
                      IconButton(
                          onPressed: () {}, icon: Icon(Icons.arrow_right))
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ]),
    );
  }
}

class MobileBerandaComponent extends StatelessWidget {
  const MobileBerandaComponent({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
        width: MediaQuery.of(context).size.width,
        color: Color.fromARGB(255, 242, 242, 242),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const HelloUser(),
              // Polis saya menu
              Container(
                margin: EdgeInsets.fromLTRB(10, 10, 0, 10),
                width: MediaQuery.of(context).size.width * 0.93,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15)),
                padding: EdgeInsets.fromLTRB(15, 12, 15, 5),
                child: Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.18,
                      child: SvgPicture.asset(
                        'assets/icons/Frame_13500.svg',
                        fit: BoxFit.fill,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Row(
                          children: [
                            Icon(
                              HomeIcon.vector,
                              size: 18,
                              color: Color.fromRGBO(110, 35, 206, 1),
                            ),
                            SizedBox(width: 10),
                            Text('Asuransi Saya',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 12)),
                          ],
                        ),
                        IconButton(
                            onPressed: () {}, icon: Icon(Icons.arrow_right))
                      ],
                    )
                  ],
                ),
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                // Form menu
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
                  child: Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 0.15,
                        child: SvgPicture.asset(
                          'assets/icons/formulir_beranda.svg',
                          fit: BoxFit.fill,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Row(
                            children: [
                              Icon(HomeIcon.doct,
                                  size: 18,
                                  color: Color.fromRGBO(68, 65, 251, 1)),
                              SizedBox(width: 10),
                              Text('Formulir',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                            ],
                          ),
                          IconButton(
                              onPressed: () {}, icon: Icon(Icons.arrow_right))
                        ],
                      )
                    ],
                  ),
                ),
                const Padding(
                    padding: EdgeInsets.symmetric(vertical: 0, horizontal: 5)),
                // e-payment menu
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
                  child: Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 0.15,
                        child: SvgPicture.asset(
                          'assets/icons/epayment_menu.svg',
                          fit: BoxFit.fill,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Row(
                            children: [
                              Icon(
                                HomeIcon.card,
                                size: 18,
                                color: Color.fromRGBO(231, 178, 80, 1),
                              ),
                              SizedBox(width: 10),
                              Text('E-payment',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                            ],
                          ),
                          IconButton(
                              onPressed: () {}, icon: Icon(Icons.arrow_right))
                        ],
                      )
                    ],
                  ),
                ),
              ]),
              const Padding(
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0)),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                // Klaim menu
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
                  child: Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 0.15,
                        child: SvgPicture.asset(
                          'assets/icons/klaim_beranda.svg',
                          fit: BoxFit.fill,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Row(
                            children: [
                              Icon(
                                HomeIcon.doc_shield,
                                size: 18,
                                color: Color.fromRGBO(252, 131, 175, 1),
                              ),
                              SizedBox(width: 10),
                              Text('Klaim Saya',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                            ],
                          ),
                          IconButton(
                              onPressed: () {}, icon: Icon(Icons.arrow_right))
                        ],
                      )
                    ],
                  ),
                ),
                const Padding(
                    padding: EdgeInsets.symmetric(vertical: 0, horizontal: 5)),
                // Web avrist menu
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15)),
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
                  child: Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 0.15,
                        child: SvgPicture.asset(
                          'assets/icons/web_menu.svg',
                          fit: BoxFit.fill,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Row(
                            children: [
                              Icon(
                                HomeIcon.logo_icon,
                                size: 18,
                                color: Color.fromRGBO(167, 175, 172, 1),
                              ),
                              SizedBox(width: 10),
                              Text('Web Avrist',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12)),
                            ],
                          ),
                          IconButton(
                              onPressed: () {}, icon: Icon(Icons.arrow_right))
                        ],
                      )
                    ],
                  ),
                ),
              ])
            ],
          ),
        ));
  }
}
