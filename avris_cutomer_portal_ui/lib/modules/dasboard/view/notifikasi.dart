import 'package:avris_cutomer_portal_ui/core/utils/responsive.dart';
import 'package:flutter/material.dart';

class Notifikasi extends StatelessWidget {
  const Notifikasi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<int>(
        tooltip: '',
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        offset: Offset(0, 42),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(children: [
              Icon(
                Icons.notifications_outlined,
                color: Colors.black,
              ),
              Positioned(
                right: 3,
                child: Container(
                  height: 6,
                  width: 6,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.red,
                  ),
                ),
              )
            ]),
            Padding(padding: EdgeInsets.only(left: 3)),
            if (Responsive.isDesktop(context))
              Text('Notifikasi', style: TextStyle(color: Colors.black)),
          ],
        ),
        itemBuilder: (BuildContext context) {
          return [
            PopupMenuItem<int>(
              padding: const EdgeInsets.fromLTRB(15, 5, 0, 5),
              enabled: false,
              value: 0,
              child: SizedBox(
                  width: 250,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text('Notifikasi',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold)),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 3, horizontal: 8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Color.fromRGBO(115, 103, 240, 0.12)),
                              child: Text('7 baru',
                                  style: TextStyle(
                                      color: Color.fromRGBO(115, 103, 240, 1),
                                      fontSize: 12)),
                            ),
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 10),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 4,
                                      offset: Offset(
                                          0, 2), // changes position of shadow
                                    ),
                                  ],
                                  gradient: LinearGradient(colors: [
                                    Color.fromRGBO(89, 45, 131, 1),
                                    Color.fromRGBO(162, 112, 226, 1)
                                  ]),
                                  borderRadius: BorderRadius.circular(6)),
                              child: TextButton(
                                  onPressed: () {},
                                  child: const Text('Belum dibaca',
                                      style: TextStyle(
                                          fontSize: 10, color: Colors.white))),
                            ),
                            Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(243, 243, 243, 1),
                                    borderRadius: BorderRadius.circular(6)),
                                child: TextButton(
                                    onPressed: () {},
                                    child: const Text(
                                      'Sudah dibaca',
                                      style: TextStyle(
                                          fontSize: 10, color: Colors.black),
                                    )))
                          ],
                        )
                      ])),
            ),
            const PopupMenuDivider(),
            PopupMenuItem<int>(
              padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
              value: 1,
              child: SizedBox(
                // height: 125,
                width: 250,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Text('Biling reminder',
                              style: TextStyle(fontSize: 12)),
                          Text('2304100001', style: TextStyle(fontSize: 12))
                        ],
                      ),
                      const SizedBox(height: 20),
                      const Text(
                          'Lorem ipsum dolor sit amet consectetur. Est sit nunc.',
                          style: TextStyle(fontSize: 12)),
                      const SizedBox(height: 15),
                      const Text('12 April 2023, 08:13',
                          style: TextStyle(
                              fontSize: 10,
                              color: Color.fromRGBO(190, 191, 207, 1))),
                    ]),
              ),
            ),
            const PopupMenuDivider(),
          ];
        });
  }
}
