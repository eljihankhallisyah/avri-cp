import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileComponent extends StatelessWidget {
  const ProfileComponent({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(30, 10, 30, 20),
          child: Row(
            children: [
              Icon(Icons.arrow_back),
              Padding(padding: EdgeInsets.only(left: 10)),
              Text('Profil',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                margin: EdgeInsets.only(right: 15),
                padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                width: MediaQuery.of(context).size.width * 0.25,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.10),
                        blurRadius: 10,
                      )
                    ]),
                child: Center(
                    child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(15),
                      child: SvgPicture.asset('assets/icons/Frame_bear.svg',
                          width: 100, height: 100),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: Text('Deavid Rizky Yolandra',
                          style: TextStyle(fontWeight: FontWeight.w600)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('ID  ',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 12)),
                          Text('C010583300PYR', style: TextStyle(fontSize: 12))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.all(3),
                            margin: EdgeInsets.only(right: 6),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(228, 226, 243, 1),
                                borderRadius: BorderRadius.circular(5)),
                            child: Icon(Icons.male_rounded,
                                size: 16,
                                color: Color.fromRGBO(89, 45, 131, 1)),
                          ),
                          Text('Pria', style: TextStyle(fontSize: 12)),
                          Container(
                            padding: EdgeInsets.all(5),
                            margin: EdgeInsets.fromLTRB(10, 0, 6, 0),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(228, 226, 243, 1),
                                borderRadius: BorderRadius.circular(5)),
                            child: Icon(Icons.calendar_month_rounded,
                                size: 14,
                                color: Color.fromRGBO(89, 45, 131, 1)),
                          ),
                          Text('02/12/1993', style: TextStyle(fontSize: 12))
                        ],
                      ),
                    )
                  ],
                ))),
            Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 10),
                width: MediaQuery.of(context).size.width * 0.45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.10),
                        blurRadius: 10,
                      )
                    ]),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Info',
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16)),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 3),
                      child: Divider(),
                    ),
                    Table(
                      defaultVerticalAlignment:
                          TableCellVerticalAlignment.middle,
                      columnWidths: const <int, TableColumnWidth>{
                        0: FractionColumnWidth(0.5),
                        1: FractionColumnWidth(0.5),
                      },
                      children: const [
                        TableRow(children: <Widget>[
                          Text('No. KTP:',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Color.fromRGBO(110, 107, 123, 1))),
                          Text('No. Handphone:',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Color.fromRGBO(110, 107, 123, 1))),
                        ]),
                        TableRow(children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 6),
                            child: Text('123456789012345',
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(110, 107, 123, 1))),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 6),
                            child: Text('0821 8654 4204',
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(110, 107, 123, 1))),
                          ),
                        ]),
                        TableRow(children: <Widget>[
                          Text('Email:',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Color.fromRGBO(110, 107, 123, 1))),
                          Text('Telp Kantor',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Color.fromRGBO(110, 107, 123, 1))),
                        ]),
                        TableRow(children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 6),
                            child: Text('deavid@gmail.com',
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(110, 107, 123, 1))),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 6),
                            child: Text('(021) 7711502',
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(110, 107, 123, 1))),
                          ),
                        ]),
                        TableRow(children: <Widget>[
                          Text('Telp rumah',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Color.fromRGBO(110, 107, 123, 1))),
                          Text('Provinsi',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Color.fromRGBO(110, 107, 123, 1))),
                        ]),
                        TableRow(children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 6),
                            child: Text('-',
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(110, 107, 123, 1))),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 6),
                            child: Text('Jawa Barat',
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(110, 107, 123, 1))),
                          ),
                        ]),
                        TableRow(children: <Widget>[
                          Text('Kota',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Color.fromRGBO(110, 107, 123, 1))),
                          Text('Alamat rumah',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12,
                                  color: Color.fromRGBO(110, 107, 123, 1))),
                        ]),
                        TableRow(children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 6),
                            child: Text('Jakarta Timur',
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(110, 107, 123, 1))),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 6),
                            child: Text(
                                'Lorem ipsum dolor sit amet consectetur. Volutpat enim vitae sit ipsum arcu tempus',
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(110, 107, 123, 1))),
                          ),
                        ]),
                      ],
                    )
                  ],
                ))
          ],
        )
      ],
    ));
  }
}
