import 'package:avris_cutomer_portal_ui/core/utils/logger.dart';
import 'package:avris_cutomer_portal_ui/modules/dasboard/view/notifikasi.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationBarDesktop extends StatefulWidget {
  final Function(int) callback;
  String? stringValue;
  NotificationBarDesktop({Key? key, required this.callback}) : super(key: key);

  @override
  State<NotificationBarDesktop> createState() => _NotificationBarState();
}

class _NotificationBarState extends State<NotificationBarDesktop> {
  // Future<void> data() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   widget.stringValue = prefs.getString("username");
  //   // logger.d('ss', widget.stringValue);
  // }

  @override
  void initState() {
    super.initState();
    // data();
    SharedPreferences.getInstance().then((prefs) {
      setState(() => widget.stringValue = prefs.getString("username"));
    });
    // logger.d('aa', widget.stringValue);
  }

  @override
  Widget build(BuildContext context) {
    final user = widget.stringValue;
    return Container(
      margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
      height: 50,
      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.10),
              blurRadius: 10,
            )
          ]),
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const CountryCodePicker(
              showDropDownButton: false,
              hideSearch: true,
              initialSelection: 'ID',
              countryFilter: ['ID', 'GB'],
              showCountryOnly: true,
              showOnlyCountryWhenClosed: true,
              alignLeft: false,
              dialogSize: Size(200, 200),
            ),
            Row(
              children: [
                Container(
                    margin: EdgeInsets.only(right: 20), child: Notifikasi()),
                Container(
                    child: PopupMenuButton<int>(
                        tooltip: '',
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        offset: Offset(10, 50),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CircleAvatar(
                              backgroundColor: Colors.transparent,
                              child: SvgPicture.asset(
                                  'assets/icons/Frame_650.svg'),
                            ),
                            Padding(padding: EdgeInsets.only(left: 3)),
                            Text(user.toString(),
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12)),
                          ],
                        ),
                        itemBuilder: (BuildContext context) {
                          return [
                            PopupMenuItem<int>(
                              value: 1,
                              child: SizedBox(
                                width: 180,
                                child: Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      child: SvgPicture.asset(
                                        'assets/icons/Frame_650.svg',
                                        width: 25,
                                        height: 25,
                                      ),
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 3)),
                                    const Text('Profil',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 13)),
                                  ],
                                ),
                              ),
                              onTap: () {
                                widget.callback(6);
                              },
                            ),
                            PopupMenuItem<int>(
                              padding: const EdgeInsets.only(left: 25),
                              value: 2,
                              child: SizedBox(
                                width: 180,
                                child: Row(
                                  children: const [
                                    Icon(Icons.vpn_key_outlined,
                                        size: 20,
                                        color:
                                            Color.fromRGBO(190, 191, 207, 1)),
                                    Padding(padding: EdgeInsets.only(left: 12)),
                                    Text('Ubah password',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 13)),
                                  ],
                                ),
                              ),
                            ),
                            PopupMenuItem<int>(
                              padding: const EdgeInsets.only(left: 25),
                              value: 3,
                              child: SizedBox(
                                width: 180,
                                child: Row(
                                  children: const [
                                    Icon(Icons.exit_to_app_outlined,
                                        size: 20,
                                        color:
                                            Color.fromRGBO(190, 191, 207, 1)),
                                    Padding(padding: EdgeInsets.only(left: 12)),
                                    Text('Keluar',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 13)),
                                  ],
                                ),
                              ),
                              onTap: () {
                                widget.callback(7);
                              },
                            ),
                          ];
                        }))
              ],
            )
          ]),
    );
  }
}
