import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HelloUser extends StatelessWidget {
  const HelloUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var t = AppLocalizations.of(context);
    return Container(
        margin: const EdgeInsets.fromLTRB(30, 10, 10, 6),
        child: Text(t.helloWorld));
  }
}
