import 'package:avris_cutomer_portal_ui/core/utils/background.dart';
import 'package:avris_cutomer_portal_ui/core/utils/responsive.dart';
import 'package:avris_cutomer_portal_ui/modules/login/view/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class RegisterSuccess extends StatelessWidget {
  const RegisterSuccess({Key? key}) : super(key: key);
  static const routeName = "/berhasil-mendaftar";

  @override
  Widget build(BuildContext context) {
    return const Background(
        child: SingleChildScrollView(
            child: SafeArea(
                child: Responsive(
      mobile: RegisterSuccessScreen(),
      desktop: RegisterSuccessScreen(),
    ))));
  }
}

class MobileRegisterSuccessScreen extends StatelessWidget {
  const MobileRegisterSuccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: const [
            Spacer(),
            Expanded(
              flex: 8,
              child: RegisterSuccessScreen(),
            ),
            Spacer(),
          ],
        ),
      ],
    );
  }
}

class RegisterSuccessScreen extends StatelessWidget {
  const RegisterSuccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            width: 350,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Berhasil mendaftar",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Padding(padding: EdgeInsets.only(bottom: 15)),
                RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                      style: const TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            text:
                                'Kami telah mengirimkan username & password sementara ke email '),
                        TextSpan(
                            text: 'deavid@gmail.com ',
                            style:
                                const TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(text: 'segera reset password kamu'),
                      ],
                    )),
              ],
            )),
        Container(
          width: 400,
          child: SvgPicture.asset(
            "assets/icons/Frame_639.svg",
            fit: BoxFit.fill,
          ),
        ),
        Container(
          width: 350,
          decoration: BoxDecoration(
            gradient: const LinearGradient(
              colors: [
                Color.fromRGBO(89, 45, 131, 1),
                Color.fromRGBO(162, 112, 226, 1),
              ], // set the colors for the gradient
              begin: Alignment
                  .bottomCenter, // set the starting position of the gradient
              end: Alignment
                  .topCenter, // set the ending position of the gradient
            ),
            borderRadius: BorderRadius.circular(6), // set border radius
          ),
          child: ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const LoginScreen();
                }));
              },
              child: const Text('Oke',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ))),
        )
      ],
    );
  }
}
