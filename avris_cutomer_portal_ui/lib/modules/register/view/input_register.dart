import 'package:avris_cutomer_portal_ui/core/utils/logger.dart';
import 'package:avris_cutomer_portal_ui/modules/login/view/login_screen.dart';
import 'package:avris_cutomer_portal_ui/modules/otp/view/otp_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_phone_field/form_builder_phone_field.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

class InputRegister extends StatefulWidget {
  const InputRegister({super.key});

  @override
  State<InputRegister> createState() => _InputRegisterState();
}

class _InputRegisterState extends State<InputRegister> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _dateController = TextEditingController();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? selectedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );
    if (selectedDate != null) {
      setState(() {
        _dateController.text = DateFormat('dd-MM-yyyy').format(selectedDate);
      });
    }
  }

  bool _passwordVisible = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.9,
        child: Stack(
          children: <Widget>[
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(
                children: [
                  IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Get.offNamed(LoginScreen.routeName);
                      }),
                  const Text(
                    'Daftar',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  )
                ],
              ),
            ]),
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FormBuilder(
                      key: _formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 10),
                              alignment: Alignment.center,
                              color: Colors.white,
                              child: ListTile(
                                subtitle: FormBuilderTextField(
                                  name: "polis",
                                  decoration: const InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    hintText: 'Nomor polis',
                                    isDense: true,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 10),
                              child: ListTile(
                                  subtitle: FormBuilderTextField(
                                name: "dob",
                                controller: _dateController,
                                readOnly: true,
                                onTap: () => _selectDate(context),
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  hintText: 'Pilih tanggal',
                                  suffixIcon: Icon(Icons.calendar_today),
                                  isDense: true,
                                ),
                              )),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 10),
                              child: ListTile(
                                subtitle: Container(
                                    child: FormBuilderPhoneField(
                                  name: 'phone_number',
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    hintText: '821 8654 xxxx',
                                    isDense: true,
                                    counterText: "",
                                    errorStyle: TextStyle(
                                      color: Colors.transparent,
                                      fontSize: 0,
                                    ),
                                  ),
                                  priorityListByIsoCode: ['ID', 'US'],
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(),
                                  ]),
                                )),
                              ),
                            ),
                            // Container(
                            //   margin: EdgeInsets.only(bottom: 10),
                            //   child: ListTile(
                            //     subtitle: IntlPhoneField(
                            //       decoration: InputDecoration(
                            //         filled: true,
                            //         fillColor: Colors.white,
                            //         border: OutlineInputBorder(
                            //             borderRadius: BorderRadius.all(
                            //                 Radius.circular(5))),
                            //         hintText: '821 8654 xxxx',
                            //         isDense: true,
                            //         counterText: "",
                            //         errorStyle: TextStyle(
                            //           color: Colors.transparent,
                            //           fontSize: 0,
                            //         ),
                            //       ),
                            //       initialCountryCode: 'ID',
                            //       disableLengthCheck: true,
                            //     ),
                            //   ),
                            // ),
                            Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: ListTile(
                                  subtitle: FormBuilderTextField(
                                    name: "email",
                                    decoration: const InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      prefixIcon: Icon(Icons.email),
                                      labelText: 'Email',
                                      isDense: true,
                                    ),
                                  ),
                                )),
                            Container(
                              margin: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color.fromRGBO(89, 45, 131, 1),
                                    Color.fromRGBO(162, 112, 226, 1),
                                  ], // set the colors for the gradient
                                  begin: Alignment
                                      .bottomCenter, // set the starting position of the gradient
                                  end: Alignment
                                      .topCenter, // set the ending position of the gradient
                                ),
                                borderRadius: BorderRadius.circular(
                                    10), // set border radius
                              ),
                              child: Center(
                                child: ElevatedButton(
                                  onPressed: submit,
                                  // onPressed: () {
                                  // Get.offNamed(OtpForm.routeName);
                                  // Get.to(() => OtpForm.routeName,
                                  //     arguments: {'number': '085266723040'});
                                  // Navigator.pushNamed(
                                  //     context, OtpForm.routeName,
                                  //     arguments: {'number': '085266723040'});
                                  // },
                                  child: Text(
                                    'Daftar',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ]))
                ])
          ],
        ));
  }

  void submit() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      final formData = _formKey.currentState!.value;
      logger.i(formData);
    }
  }
}
