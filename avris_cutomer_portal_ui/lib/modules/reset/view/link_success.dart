import 'package:avris_cutomer_portal_ui/core/utils/background.dart';
import 'package:avris_cutomer_portal_ui/core/utils/responsive.dart';
import 'package:avris_cutomer_portal_ui/modules/login/view/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class LinkSuccess extends StatelessWidget {
  const LinkSuccess({Key? key}) : super(key: key);
  static const routeName = "/link-success";

  @override
  Widget build(BuildContext context) {
    return const Background(
        child: SingleChildScrollView(
            child: SafeArea(
                child: Responsive(
      mobile: MobileLinkSuccessScreen(),
      desktop: LinkSuccessScreen(),
    ))));
  }
}

class MobileLinkSuccessScreen extends StatelessWidget {
  const MobileLinkSuccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: const [
            Spacer(),
            Expanded(
              flex: 8,
              child: LinkSuccessScreen(),
            ),
            Spacer(),
          ],
        ),
      ],
    );
  }
}

class LinkSuccessScreen extends StatelessWidget {
  const LinkSuccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            width: 400,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  "Berhasil kirim link reset password",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Padding(padding: EdgeInsets.only(bottom: 15)),
                Text(
                  "Kami telah kirim link reset password ke",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal),
                ),
                Text(
                  "david@gmail.com",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            )),
        Container(
          width: 450,
          child: SvgPicture.asset(
            "assets/icons/Frame_13479.svg",
            fit: BoxFit.fill,
          ),
        ),
        Container(
          width: 400,
          decoration: BoxDecoration(
            gradient: const LinearGradient(
              colors: [
                Color.fromRGBO(89, 45, 131, 1),
                Color.fromRGBO(162, 112, 226, 1),
              ], // set the colors for the gradient
              begin: Alignment
                  .bottomCenter, // set the starting position of the gradient
              end: Alignment
                  .topCenter, // set the ending position of the gradient
            ),
            borderRadius: BorderRadius.circular(6), // set border radius
          ),
          child: ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const LoginScreen();
                }));
              },
              child: const Text('Oke',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ))),
        )
      ],
    );
  }
}
