import 'package:avris_cutomer_portal_ui/core/utils/home_icon_icons.dart';
import 'package:avris_cutomer_portal_ui/modules/polis_saya/view/polisDetail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PolisMenu extends StatelessWidget {
  final Function(int) callback;
  PolisMenu({Key? key, required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(30, 5, 30, 5),
            child: const Row(
              children: [
                Icon(Icons.arrow_back),
                Padding(padding: EdgeInsets.only(left: 10)),
                Text('Asuransi saya',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))
              ],
            ),
          ),
          SizedBox(height: 10),
          Center(
            child: Container(
              constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.75),
              padding: EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(7)),
              child: Column(
                children: [
                  for (var i = 0; i < 2; i++)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        for (var i = 0; i < 3; i++)
                          Container(
                            constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.23),
                            padding: EdgeInsets.all(15),
                            margin: EdgeInsets.symmetric(
                                horizontal: 5, vertical: 8),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey.shade300),
                                borderRadius: BorderRadius.circular(7)),
                            child: Column(
                              children: [
                                PolisBox(),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8),
                                  child: MaterialButton(
                                      hoverColor: Colors.transparent,
                                      focusColor: Colors.transparent,
                                      onPressed: () {
                                        callback(1);
                                      },
                                      child: Container(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 8),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            border: Border.all(
                                                color: Color.fromRGBO(
                                                    162, 112, 226, 1))),
                                        child: Center(
                                          child: Text('Lihat Detail',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Color.fromRGBO(
                                                      89, 45, 131, 1))),
                                        ),
                                      )),
                                )
                              ],
                            ),
                          )
                      ],
                    )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class PolisMenuMobile extends StatelessWidget {
  final Function(int) callback;
  PolisMenuMobile({Key? key, required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        Center(
          child: Container(
            // constraints: BoxConstraints(
            //     maxWidth: MediaQuery.of(context).size.width * 0.75),
            padding: EdgeInsets.symmetric(vertical: 20),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(7)),
            child: Column(
              children: [
                for (var i = 0; i < 3; i++)
                  Container(
                    // constraints: BoxConstraints(
                    //     maxWidth: MediaQuery.of(context).size.width * 0.23),
                    padding: EdgeInsets.all(15),
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 8),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey.shade300),
                        borderRadius: BorderRadius.circular(7)),
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Column(
                      children: [
                        PolisBox(),
                        Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: MaterialButton(
                              hoverColor: Colors.transparent,
                              focusColor: Colors.transparent,
                              onPressed: () {
                                callback(1);
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                        color:
                                            Color.fromRGBO(162, 112, 226, 1))),
                                child: Center(
                                  child: Text('Lihat Detail',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color:
                                              Color.fromRGBO(89, 45, 131, 1))),
                                ),
                              )),
                        )
                      ],
                    ),
                  )
              ],
            ),
          ),
        )
      ],
    ));
    // return Column(
    //   mainAxisAlignment: MainAxisAlignment.center,
    //   children: <Widget>[
    //     Row(
    //       children:  [
    //         Spacer(),
    //         Expanded(
    //           flex: 8,
    //           child: PolisMenu(callback: 0),
    //         ),
    //         Spacer(),
    //       ],
    //     ),
    //   ],
    // );
  }
}

class PolisBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: const <int, TableColumnWidth>{
          0: FractionColumnWidth(0.5),
          1: FractionColumnWidth(0.5),
        },
        children: [
          TableRow(children: <Widget>[
            Row(
              children: [
                const Text('No Polis',
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.w800)),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  padding: EdgeInsets.symmetric(horizontal: 7, vertical: 3),
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(40, 199, 111, 0.12)),
                  child: Text('Aktif',
                      style: TextStyle(
                        color: Color.fromRGBO(40, 199, 111, 1),
                        fontSize: 11,
                      )),
                ),
              ],
            ),
            const Text('1234567',
                textAlign: TextAlign.right, style: TextStyle(fontSize: 12)),
          ]),
          const TableRow(children: <Widget>[
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 6),
                  child: Divider(),
                ),
              ],
            ),
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 6),
                  child: Divider(),
                ),
              ],
            )
          ]),
          TableRow(children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Row(
                children: [
                  Icon(
                    HomeIcon.doc_shield,
                    size: 20,
                    color: Colors.grey.shade500,
                  ),
                  SizedBox(width: 5),
                  const Text('Product',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.left),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(bottom: 4),
              child: Text('Avrist Investment Plus',
                  textAlign: TextAlign.right, style: TextStyle(fontSize: 12)),
            ),
          ]),
          TableRow(children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Row(
                children: [
                  Icon(
                    HomeIcon.vector,
                    size: 20,
                    color: Colors.grey.shade500,
                  ),
                  const SizedBox(width: 5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Nama',
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.left),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            maxWidth:
                                MediaQuery.of(context).size.width * 0.068),
                        child: Text(
                          'pemegang polis',
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Text('Brad Pit',
                  textAlign: TextAlign.right, style: TextStyle(fontSize: 12)),
            ),
          ]),
          TableRow(children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Row(
                children: [
                  Icon(
                    Icons.calendar_today,
                    size: 20,
                    color: Colors.grey.shade500,
                  ),
                  const SizedBox(width: 5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Tanggal',
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.left),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            maxWidth:
                                MediaQuery.of(context).size.width * 0.068),
                        child: Text(
                          'berlaku polis',
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Text('12/12/2023',
                  textAlign: TextAlign.right, style: TextStyle(fontSize: 12)),
            ),
          ])
        ]);
  }
}
