import 'package:avris_cutomer_portal_ui/core/utils/home_icon_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PolisDetail extends StatefulWidget {
  final Function(int) callback;
  PolisDetail({Key? key, required this.callback}) : super(key: key);
  @override
  State<PolisDetail> createState() => _PolisDetailState();
}

class _PolisDetailState extends State<PolisDetail> {
  int _selectedIndex = 0;

  void setIndex(index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(children: [
      Container(
        margin: EdgeInsets.fromLTRB(30, 5, 30, 5),
        child: Row(
          children: [
            MaterialButton(
                minWidth: 50,
                onPressed: () {
                  widget.callback(0);
                },
                hoverColor: Colors.transparent,
                focusColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: Icon(Icons.arrow_back)),
            Text('Detail Polis',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15))
          ],
        ),
      ),
      SizedBox(height: 10),
      Center(
        child: Container(
          constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width * 0.75),
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(7)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (_selectedIndex == 0)
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(89, 45, 131, 1),
                            Color.fromRGBO(162, 112, 226, 1),
                          ],
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                        ),
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: Center(
                        child: MaterialButton(
                          onPressed: () {},
                          child: Text(
                            'Polis Saya',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(right: 10)),
                    Container(
                      child: Center(
                        child: MaterialButton(
                          onPressed: () {
                            setIndex(1);
                          },
                          child: Text(
                            'Transaksi',
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              if (_selectedIndex == 1)
                Row(
                  children: [
                    Container(
                      child: Center(
                        child: MaterialButton(
                          onPressed: () {
                            setIndex(0);
                          },
                          child: Text(
                            'Polis Saya',
                          ),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(right: 10)),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(89, 45, 131, 1),
                            Color.fromRGBO(162, 112, 226, 1),
                          ],
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                        ),
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: Center(
                        child: MaterialButton(
                          onPressed: () {},
                          child: Text(
                            'Transaksi',
                            style: TextStyle(color: Colors.white, fontSize: 13),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Divider(),
              ),
              if (_selectedIndex == 0)
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.all(0),
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width * 0.35),
                      child: Column(
                        children: [
                          ExpansionTile(
                            title: Text('Pemegang polis'),
                            children: [
                              Container(
                                child: Text("Pemegang polis"),
                              )
                            ],
                          ),
                          ExpansionTile(
                            title: Text('Penerima manfaat'),
                            children: [
                              Container(
                                child: Text("Penerima manfaat"),
                              )
                            ],
                          ),
                          ExpansionTile(
                            title: Text('Agen'),
                            children: [
                              Container(
                                child: Text("Agen"),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(0),
                      constraints: BoxConstraints(
                          maxWidth: MediaQuery.of(context).size.width * 0.35),
                      child: Column(
                        children: [
                          ExpansionTile(
                            title: Text("Tertanggung"),
                            children: [
                              Container(
                                child: Text("Tertanggung"),
                              )
                            ],
                          ),
                          ExpansionTile(
                            title: Text("Informasi pembayaran"),
                            children: [
                              Container(
                                child: Text("Informasi pembayaran"),
                              )
                            ],
                          ),
                          ExpansionTile(
                            title: Text("Riwayat polis"),
                            children: [
                              Container(
                                child: Text("Riwayat polis"),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              if (_selectedIndex == 0)
                Container(
                  margin: EdgeInsets.all(0),
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * 0.35),
                  child: ExpansionTile(
                    title: Text("Faedah tambahan"),
                    children: [
                      Container(
                        child: Text(
                            "Lorem ipsum dolor sit amet consectetur. Velit et quam curabitur placerat vel eget netus. Nisi semper pretium vel vel non massa. Commodo volutpat eu nibh est ut. Phasellus erat viverra enim arcu sit ante feugiat montes dolor. Odio neque nunc ac arcu lectus nulla. Malesuada tincidunt felis tincidunt mollis gravida. Maecenas dolor sed consectetur ultrices mi proin. Nascetur lectus in mauris sapien in. In eu mauris ac condimentum eu venenatis id volutpat. Laoreet egestas fringilla integer lectus neque.Libero at amet adipiscing id quam dictumst. Proin eget accumsan in cras tempor volutpat blandit feugiat. Vitae nam tempor nisl praesent. Dolor orci pellentesque iaculis vel enim dignissim. Dolor nunc est neque quis eget at ut habitant velit. Bibendum egestas et malesuada in nulla blandit. Mollis mi risus aliquet ornare. Aliquam elit quis etiam consequat erat sit at. Enim nisi id aliquet adipiscing. Sed blandit felis nullam amet ornare. Tortor in at sit consectetur hac sed.Facilisi consequat nunc sed rhoncus sit. Eget nisi enim ac velit mattis duis massa suscipit. Odio interdum.",
                            textAlign: TextAlign.justify),
                      )
                    ],
                  ),
                ),
            ],
          ),
        ),
      )
    ]));
  }
}
