import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OtpScreenInput extends StatefulWidget {
  final int length;

  const OtpScreenInput({required this.length});

  @override
  State<OtpScreenInput> createState() => _OtpScreenInputState();
}

class _OtpScreenInputState extends State<OtpScreenInput> {
  late List<TextEditingController> _controllers;
  late List<FocusNode> _focusNodes;

  @override
  void initState() {
    super.initState();

    // Initialize controllers and focus nodes
    _controllers = List.generate(
      widget.length,
      (_) => TextEditingController(),
    );
    _focusNodes = List.generate(
      widget.length,
      (_) => FocusNode(),
    );
  }

  @override
  void dispose() {
    // Dispose controllers and focus nodes
    _controllers.forEach((controller) => controller.dispose());
    _focusNodes.forEach((focusNode) => focusNode.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Build list of text fields
    List<Widget> textFields = List.generate(
      widget.length,
      (index) => Container(
        width: 50.0,
        child: TextField(
          controller: _controllers[index],
          focusNode: _focusNodes[index],
          maxLength: 1,
          keyboardType: TextInputType.number,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20.0),
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly,
            LengthLimitingTextInputFormatter(1),
          ],
          onChanged: (value) {
            if (value.isNotEmpty && index < widget.length - 1) {
              _focusNodes[index + 1].requestFocus();
            }
          },
          onSubmitted: (value) {
            if (index == widget.length - 1) {
              // Submit OTP
              String otp = _controllers.fold<String>(
                '',
                (previousValue, controller) => previousValue + controller.text,
              );
              print(otp);
            }
          },
        ),
      ),
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: textFields,
    );
  }
}
