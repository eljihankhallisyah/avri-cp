import 'package:avris_cutomer_portal_ui/core/utils/background.dart';
import 'package:avris_cutomer_portal_ui/core/utils/responsive.dart';
import 'package:avris_cutomer_portal_ui/modules/login/view/login_screen.dart';
import 'package:avris_cutomer_portal_ui/modules/register/view/success_register.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'dart:async';

class OtpForm extends StatefulWidget {
  const OtpForm({Key? key}) : super(key: key);

  static const routeName = "/otp";

  @override
  State<OtpForm> createState() => _OtpFormState();
}

class _OtpFormState extends State<OtpForm> {
  TextEditingController _otpController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool _isTimerRunning = false;
  int _timerSeconds = 30;

  late Timer _timer;
  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  void _startTimer() {
    setState(() {
      _isTimerRunning = true;
      _timerSeconds = 30;
    });

    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (_timerSeconds > 0) {
          _timerSeconds--;
        } else {
          _isTimerRunning = false;
          _timer.cancel();
        }
      });
    });
  }

  void _resendOtp() {
    _otpController.clear();
    _startTimer();
  }

  @override
  void dispose() {
    _otpController.dispose();
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // final args =
    //     ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    // final number = args['number'];
    // final number = widget.number;
    String _otpCode = '';
    return Scaffold(
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 450,
                  margin: EdgeInsets.only(top: 15),
                  alignment: Alignment.topRight,
                  child: IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Get.offNamed(LoginScreen.routeName);
                    },
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    width: 400,
                    margin: EdgeInsets.only(left: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          "OTP telah dikirim",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 15)),
                        Text(
                          "Masukan 5 Digit OTP yang dikirim ke nomor",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.normal),
                        ),
                        Text(
                          "0821 8654 XXXX",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                      ],
                    )),
                Container(
                  width: 450,
                  height: 250,
                  child: SvgPicture.asset(
                    "assets/icons/Frame 6395.svg",
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  width: 300,
                  child: PinCodeTextField(
                    length: 5,
                    obscureText: false,
                    animationType: AnimationType.fade,
                    pinTheme: PinTheme(
                      borderWidth: 2,
                      shape: PinCodeFieldShape.box,
                      borderRadius: BorderRadius.circular(7),
                      activeFillColor: Colors.white,
                      inactiveFillColor: Colors.white,
                      selectedFillColor: Colors.white,
                      selectedColor: Color(0xFF592D83),
                      inactiveColor: Colors.grey,
                      activeColor: Colors.black,
                      fieldHeight: 43,
                      fieldWidth: 43,
                    ),
                    appContext: context,
                    controller: _otpController,
                    onChanged: (value) {
                      print(value);
                    },
                    autoFocus: true,
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    onCompleted: (value) {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) {
                      //       return RegisterSuccess();
                      //     },
                      //   ),
                      // );
                    },
                  ),
                ),
                if (!_isTimerRunning)
                  TextButton(
                    style: ButtonStyle(
                      overlayColor:
                          MaterialStateProperty.all(Colors.transparent),
                    ),
                    onPressed: _resendOtp,
                    child: RichText(
                        text: const TextSpan(
                      style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(text: 'Belum menerima kode?'),
                        TextSpan(
                            text: ' Kirim',
                            style: TextStyle(
                                color: Color(0xFF592D83),
                                fontWeight: FontWeight.bold)),
                      ],
                    )),
                  )
                else
                  RichText(
                      text: TextSpan(
                    style: const TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'Kirim OTP dalam '),
                      TextSpan(
                          text: '$_timerSeconds seconds',
                          style: const TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  )),
              ],
            ),
          ]),
        ),
      ),
    );
  }
}

// class OTPScreen extends StatelessWidget {
//   const OTPScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return const Background(
//         child: SingleChildScrollView(
//             child: SafeArea(
//                 child: Responsive(
//       mobile: MobileWelcomeScreen(),
//       desktop: OtpForm(),
//     ))));
//   }
// }

// class MobileWelcomeScreen extends StatelessWidget {
//   const MobileWelcomeScreen({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.center,
//       children: <Widget>[
//         Row(
//           children: const [
//             Spacer(),
//             Expanded(
//               flex: 8,
//               child: OtpForm(),
//             ),
//             Spacer(),
//           ],
//         ),
//       ],
//     );
//   }
// }
