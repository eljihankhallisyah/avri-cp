import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_recaptcha_v3/flutter_recaptcha_v2.dart';

class InputCaptcha extends StatefulWidget {
  const InputCaptcha({super.key});

  @override
  State<InputCaptcha> createState() => _InputCaptchaState();
}

class _InputCaptchaState extends State<InputCaptcha> {
  String verifyResult = "";
  RecaptchaV2Controller recaptchaV2Controller = RecaptchaV2Controller();
  final _formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          // Positioned(child: child)
          const Text(
            'Captcha',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          SizedBox(height: 30),
          Padding(padding: EdgeInsets.all(20)),
          Container(
            child: const Text(
              'Kami mendeteksi kamu telah salah 3x, untuk keamanan silahkan isi captcha',
              style: TextStyle(fontSize: 15),
              // textAlign: TextAlign.center,
            ),
          ),
          RecaptchaV2(
            apiKey: "6LeUFuMlAAAAAOFOf5wKEk1k0hlVKTz1Y6VbeJyg",
            apiSecret: "6Leu0PwUAAAAAF7HQDjUftJ1a8lJ2ttV5j_FzeCL",
            controller: recaptchaV2Controller,
            onVerifiedError: (err) {
              print(err);
            },
            onVerifiedSuccessfully: (success) {
              recaptchaV2Controller.show();
              setState(() {
                print(success);
                if (success) {
                  verifyResult = "You've been verified successfully.";
                } else {
                  verifyResult = "Failed to verify.";
                }
              });
            },
          ),
        ])
      ],
    );
  }
}
