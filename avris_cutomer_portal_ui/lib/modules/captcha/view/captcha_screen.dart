import 'package:avris_cutomer_portal_ui/core/utils/background.dart';
import 'package:avris_cutomer_portal_ui/core/utils/responsive.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'captcha_input.dart';

class CaptchaScreen extends StatelessWidget {
  const CaptchaScreen({Key? key}) : super(key: key);
  static const routeName = "/captcha";

  @override
  Widget build(BuildContext context) {
    return Background(
        child: SingleChildScrollView(
      child: Responsive(
          mobile: const MobileCaptchaScreen(),
          desktop: Row(
            children: [
              Column(children: [
                Container(
                  margin: const EdgeInsets.all(15),
                  height: MediaQuery.of(context).size.height * 0.9,
                  width: MediaQuery.of(context).size.width * 0.65,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(249, 249, 249, 1),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Center(
                    child: Expanded(
                      child: SvgPicture.asset(
                        "assets/icons/Captcha.svg",
                      ),
                    ),
                  ),
                ),
              ]),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        width: MediaQuery.of(context).size.width * 0.35,
                        margin: EdgeInsets.all(25),
                        child: InputCaptcha())
                  ],
                ),
              )
            ],
          )),
    ));
  }
}

class MobileCaptchaScreen extends StatelessWidget {
  const MobileCaptchaScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: const [
            // Spacer(),
            // Expanded(
            //   flex: 8,
            //   child: InputPassword(),
            // ),
            // Spacer(),
          ],
        ),
      ],
    );
  }
}
