import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'package:avris_cutomer_portal_ui/core/utils/logger.dart';
import 'package:avris_cutomer_portal_ui/exception/app_exception.dart';
import 'package:get/get.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthCustomerPortalProvider extends GetConnect {
  Future<bool> login(Map<String, dynamic> data) async {
    final AppLocalizations appLoc = AppLocalizations.of(Get.context!);
    final url = "https://adev.avrist.com/cp-api/api/auth/signin";
    logger.d(" dd $data");
    final response = await httpClient.post(url, body: data);

    print("respone " + response.toString());

    if (response.statusCode == 200) {
      // Successful request
      final datarespon = response.body;

      // Process the data
      final jsonData = response.body["data"];
      logger.i(datarespon);
      final token = 'Bearer ${datarespon["token"]}';
      final username = datarespon["username"];
      final roles = List<String>.from(datarespon["roles"]);
      final permissions = List<String>.from(datarespon["permissions"]);
      //menyimpan token dengan sharedpreferences
      final prefs = await SharedPreferences.getInstance();

      prefs.setString('username', username.toString());
      prefs.setString('token', token.toString());
      prefs.setString('expired', datarespon["expired"].toString());
      prefs.setStringList('roles', roles);
      prefs.setStringList('permissions', permissions);
      return true;
    } else {
      return false;
    }
  }

  Future<void> logout() async {
    final url = "https://adev.avrist.com/cp-api/api/auth/signout";
    logger.i(url);
    try {
      final prefs = await SharedPreferences.getInstance();
      final token = prefs.getString("token");
      // String ua = AppConfig.getInstanceDefault()!.userAgent;
      final response = await httpClient.get(url);
      logger.d(response.body);
      if (response.statusCode == 200 || response.statusCode == 401) {
        String? username = prefs.getString("username");

        prefs.clear();
        prefs.setString("username", username ?? "");
      }
    } catch (e) {
      logger.i(e);
      rethrow;
    }
  }
}
